<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('admin', function (Blueprint $table) {
          $table->string('id')->index();
          $table->string('name')->index();
          $table->string('email')->index();
          $table->string('password')->index();
          $table->timestamp('created_at');
    });
  }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admin');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Route::resource('/admin/Questionnaire', 'QuestionnaireController' );
// Route::resource('/admin/answers', 'AnswersController' );

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::auth();

    Route::resource('/questionnaires', 'QuestionnaireController');
      Route::resource('/questions', 'QuestionsController');

    // Route::get('/home', 'HomeController@index');
    // Route::resource('/admin/questionnaire', 'QuestionnaireController' );
    // Route::resource('/admin/answers', 'AnswersController' );
});

// Route::auth();

Route::get('/home', 'HomeController@index');

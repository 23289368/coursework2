{{ $questionniare->title }}

{!! Form::open(array('action' => 'QuestionsController@store', 'id' => 'createquestion')) !!}
        {{ csrf_field() }}
    <div class="row large-12 columns">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('content', 'Detail:') !!}
        {!! Form::textarea('content', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Add Questions', ['class' => 'button']) !!}
    </div>
{!! Form::close() !!}
